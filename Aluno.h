/*
 * Aluno.h
 *
 *  Created on: 08/01/2017
 *      Author: alfa
 */

#ifndef ALUNO_H_
#define ALUNO_H_

class Aluno {
private:
	int mat;
	string nome;
	int idade;
public:
	Aluno(int mat, string nome, int idade);
	string getNome();
	int getMat();
	int idade();
};

#endif /* ALUNO_H_ */
